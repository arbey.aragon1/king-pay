import { Component, OnInit, ViewChild } from "@angular/core";
import { MenuController, Tabs } from "@ionic/angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../app/services/auth.service";
import { switchMap, tap } from "rxjs/operators";
import { UserService } from "../services/user.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  logInInfo = {
    email: "",
    pass: ""
  };

  signUpInfo = {
    name: "",
    email: "",
    pass: "",
    passRepeat: ""
  };
  content: any;

  @ViewChild("myTabs") tabRef: Tabs;

  constructor(
    public menuCtrl: MenuController,
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit() {}

  openPage() {}

  ionViewDidEnter() {
    this.tabRef.select("login");
  }

  logForm() {
    this.authService
      .loginWithEmailAndPass(this.logInInfo.email, this.logInInfo.pass)
      .subscribe(() => {
        console.log(this.logInInfo.email, this.logInInfo.pass);
        this.router.navigate(["/home"]);
      });
  }

  loginGoogle() {
    this.authService
      .loginWithGoogle()
      .pipe(
        switchMap(data => {
          console.log(data);
          const user = {
            name: data.user.displayName,
            uid: data.user.uid,
            email: data.user.email
          };
          console.log(user);
          return this.userService.createUser(user);
        })
      )
      .subscribe(data => {
        console.log("LoginGoogle");
        this.router.navigate(["/home"]);
      });
  }

  loginFacebook() {
    console.log("LoginFacebook");
  }

  rememberMe(event: any) {
    console.log("Recuerdame");
  }

  registerForm() {
    if (this.signUpInfo.pass === this.signUpInfo.passRepeat) {
      this.authService
        .createUserWithEmailAndPass(this.signUpInfo.email, this.signUpInfo.pass)
        .pipe(
          switchMap(data => {
            console.log(data);
            const user = {
              name: this.signUpInfo.name,
              uid: data.user.uid,
              email: data.user.email
            };
            console.log(user);
            return this.userService.createUser(user);
          })
        )
        .subscribe(() => {
          console.log(this.signUpInfo);
          this.router.navigate(["/home"]);
        });
    }
  }
}
