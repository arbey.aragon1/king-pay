import { Component } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { AuthService } from "../app/services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  public appPages = [
    {
      title: "Detalle de usuario",
      url: "/user-detail",
      icon: "person"
    },
    {
      title: "Lista de facturas",
      url: "/invoice-list",
      icon: "document"
    },
    {
      title: "Historico de pagos",
      url: "/payment-list",
      icon: "pricetags"
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logOut() {
    this.authService.logout().subscribe(() => {
      console.log("logOut");
      this.router.navigate(["/login"]);
    });
  }
}
