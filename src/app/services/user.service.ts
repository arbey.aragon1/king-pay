import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import * as constants from "../shared/constants";
import { defineBase } from "@angular/core/src/render3";
import { Observable, from } from "rxjs";
import {} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private reference: any;

  constructor(private db: AngularFireDatabase) {}

  createUser(user: any): Observable<any> {
    return from(this.db.database.ref(constants.USERS_KEY).push(user));
  }
}
