import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase/app";
import { from, Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private isLoggedIn = false;

  constructor(private afAuth: AngularFireAuth, private router: Router) {}

  loginWithEmailAndPass(email: string, pass: string): Observable<any> {
    return from(
      this.afAuth.auth.signInAndRetrieveDataWithEmailAndPassword(email, pass)
    ).pipe(
      tap(() => {
        this.isLoggedIn = true;
      })
    );
  }

  loginWithGoogle(): Observable<any> {
    return from(
      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
    ).pipe(
      tap(() => {
        this.isLoggedIn = true;
      })
    );
  }

  createUserWithEmailAndPass(email: string, pass: string): Observable<any> {
    return from(
      this.afAuth.auth.createUserWithEmailAndPassword(email, pass)
    ).pipe(
      tap(() => {
        this.isLoggedIn = true;
      })
    );
  }

  logout(): Observable<any> {
    return from(this.afAuth.auth.signOut()).pipe(
      tap(() => {
        this.isLoggedIn = false;
      })
    );
  }

  authenticated(): boolean {
    return this.isLoggedIn;
  }
}
