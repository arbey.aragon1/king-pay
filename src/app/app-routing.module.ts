import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {
    path: 'payment-list',
    loadChildren: './payment-list/payment-list.module#PaymentListPageModule'
  },
  {
    path: 'invoice-list',
    loadChildren: './invoice-list/invoice-list.module#InvoiceListPageModule'
  },
  { path: 'help', loadChildren: './help/help.module#HelpPageModule' },
  {
    path: 'invoice-detail',
    loadChildren:
      './invoice-detail/invoice-detail.module#InvoiceDetailPageModule'
  },
  {
    path: 'payment-detail',
    loadChildren:
      './payment-detail/payment-detail.module#PaymentDetailPageModule'
  },
  {
    path: 'user-detail',
    loadChildren: './user-detail/user-detail.module#UserDetailPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
