// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBdpzE2Zlnkj8gaYW-CP1yPrW87bhx2hM8",
    authDomain: "chat-e99da.firebaseapp.com",
    databaseURL: "https://chat-e99da.firebaseio.com",
    projectId: "chat-e99da",
    storageBucket: "chat-e99da.appspot.com",
    messagingSenderId: "802429863939"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
